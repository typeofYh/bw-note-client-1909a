/*
 * @Author: Yh
 * @LastEditors: Yh
 */
module.exports = {
  "extends": [
    "react-app",
    "react-app/jest"
  ],
  "rules": {
    "no-console": 'off',
    "no-unused-vars": "error"
  }
}