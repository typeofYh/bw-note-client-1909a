/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import React from 'react'
import ReactDOM from 'react-dom/client'
import App from '@/App'
import "@/style/common.less"
import "@/style/theme.css"
import "@/i18n"

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);
