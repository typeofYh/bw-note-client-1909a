/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { Component } from "react"

// 挂载阶段 更新阶段都会执行
// render 生成最新的虚拟dom（描述dom节点的js对象）的
class View404 extends Component {
  timer = null; // class对象的普通属性
  state = {  // 组件状态 但凡状态改变会触发视图更新
    times: this.props.startTime || 3
  };
  componentDidMount(){
    console.log('componentDidMount');
    this.timer = setInterval(() => {
      this.setState({
        times: this.state.times - 1
      }, () => {
        if(this.state.times <= 0){
          this.hanldClick();
        }
      })
    }, 1000)
  }
  componentWillUnmount(){
    clearInterval(this.timer)
    console.log('componentWillUnmount');
  }
  hanldClick = () => {
    window.location.href = "/";
  }
  render(){
    const {times} = this.state;
    console.log('render', times);
    return (
      <div>您的页面找不到啦，{times}秒自动跳转首页，<button onClick={this.hanldClick}>点击按钮跳转首页</button></div>
    )
  }
}


export default View404;