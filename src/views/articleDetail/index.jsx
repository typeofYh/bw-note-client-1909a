/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { useParams } from "react-router-dom"
import { useStore } from "@/store/Provider"
import BaseComment from "coms/baseComment"
import BaseShare from "coms/baseShare"
import BaseCard from "coms/baseCard"
import { message } from "antd"
import { recommendList } from "@/api/article"
import { useEffect, useState, useRef } from "react"
import { getDetaildData } from "@/api/article"
import { formatTime, formatHTMLAddCopyBtn } from "@/utils/utils"
import _ from "lodash"
import cls from "classnames"
import copy from "clipboard-copy"
import "./style.less"
import "@/style/markdown.css"
const ArticleDetailView = () => {
  const { id } = useParams();  // 获取详情id
  const [detailData, setDetailData] = useState({});
  const [flowIndex, setFlowIndex] = useState(0); // 楼层
  const { commentModule } = useStore();
  const markdownRef = useRef(null);
  const init = async () => {
    try {
      const { data } = await getDetaildData(id);
      data.html = formatHTMLAddCopyBtn(data.html);
      data.tocData = JSON.parse(data.toc);
      setDetailData(data);
    } catch(error){
      console.error(error);
    }
  };
  useEffect(() => {
    init()
  }, [id])

  useEffect(() => {
    if(detailData?.tocData?.length){ // 楼层
      const offsetTops = detailData.tocData.map(item => {
        const el = document.getElementById(item.id);
        return {
          el,
          offsetTop: el.offsetTop
        }
      })
      window.addEventListener('scroll', window.scrollEvent = _.debounce(() => {
        const scrollTop = document.documentElement.scrollTop;
        console.log(offsetTops, scrollTop);
        let curIndex = 0;
        offsetTops.forEach(({offsetTop},i) => {
          if(i === offsetTops.length - 1 && scrollTop > offsetTop){
            curIndex = i
            return;
          }
          if(scrollTop >= offsetTop && scrollTop < offsetTops[i + 1].offsetTop){
            curIndex = i
          }
        })
        setFlowIndex(curIndex);
        // 修改楼层id setFlowIndex
      }, 100))
    }
    return () => { // 一定要卸载事件！！！！！
      window.removeEventListener('scroll', window.scrollEvent);
    }
  },[markdownRef, detailData])

  const hanldDetailContentClick = (e) => {
    if(e.target.classList.contains('copy-code-btn')){
      const copyContent = e.target.nextElementSibling.innerHTML;
      // console.log(copyContent);
      copy(copyContent)
      message.success('复制成功');
    }
  }
  
  const handleClickFlow = (index, item) => {
    setFlowIndex(index); // 改变楼层
    const titleTag = document.getElementById(item.id);
    document.documentElement.scrollTo(0, titleTag.offsetTop);
  }
  return (
    <div className={"detail-wrapper wrapper"}>
      {/**分享侧边栏 */}
      <BaseShare />
      <div className={"detail-content leftWrapper"}>
        {/**详情内容 */}
        <div>
          <h1>{detailData?.title}</h1>
          <h2>{formatTime(detailData?.publishAt)} · 阅读量 {detailData?.views}</h2>
          <div 
            onClick={hanldDetailContentClick} 
            className={'markdown'} 
            ref={markdownRef}
            dangerouslySetInnerHTML={{__html:detailData?.html}}
          ></div>
        </div>
        {/**评论组件 */}
        <BaseComment 
          pageInfo = {{ // 启用分页
            page:1,
            pageSize:3,
            size: 'small'
          }}
          request={async ({ pageConfig }) => {  // 发送请求 组件创建 分页改变
            const { data } =  await commentModule.getCommentList(id, pageConfig);
            return {
              data: data[0], // 当前页数据
              total: data[1]  // 总条数
            }
          }}
        />
      </div>
      {/** 右侧*/}
      <div className={"detail-right-slide rightWrapper"}>
        <BaseCard 
          title="推荐阅读" 
          request={async () => {
            const { data } = await recommendList({
              articleId: id
            });
            return {
              data: data.slice(0, 6)
            }
          }}
        >
          {
            (item) => {
              return <p key={item.id}>{item.title}</p>
            }
          }
        </BaseCard>
        {/**楼层导航 侧边栏 */}
        <div className="flow">
          <ul>
            {
              Array.isArray(detailData?.tocData) && detailData.tocData.map((item,index) => (
                <li 
                  className={cls({
                    active: flowIndex === index
                  })} 
                  style={{paddingLeft: (item.level - 1) * 10, cursor: 'pointer'}} 
                  key={item.id}
                  onClick={() => handleClickFlow(index, item)}
                >{item.text}</li>
              ))
            }
          </ul>
        </div>
      </div>
    </div>
  )
}

export default ArticleDetailView;