/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import EditorComment from "./components/editor"
import ListComment from "./components/list"
import { Pagination } from "antd"
import { useEffect, useState } from "react"
const BaseComment = ({request = () => {}, pageInfo = null}) => {
  const [commentList, setCommentList] = useState([]); // 评论数据
  const [loading, setLoading] = useState(false);
  const [total, setTotal] = useState(0);
  const [pageConfig, setPageConfig] = useState({  // 分页数据
    page:pageInfo?.page || 1,
    pageSize: pageInfo?.pageSize || 10
  })
  useEffect(() => {
    setLoading(true);
    const res = request({ pageConfig }); // 调用request
    if(!(res instanceof Promise)) {
      throw new Error(`request 返回值必须是promise实例`)
    }
    res.then(val => {
      setLoading(false);
      setCommentList(val.data); // 设置评论数据
      setTotal(val.total);
    })
  },[request, pageConfig])


  const handleChange = (page, pageSize) => {
    // console.log(page, pageSize, setPageConfig);
    setPageConfig({
      page,
      pageSize
    })
  }
  const handleShowEditor = (item) => {
    item.isShowEditor = !item.isShowEditor;
    setCommentList((val) => [...val]);
  }
  return (
    <>
      <h2>评论</h2>
      <EditorComment 
        placeholder={'请输入评论内容（支持 Markdown）'} 
        onClickShowEditor={handleShowEditor}
      />
      <ListComment 
        lists={commentList} 
        onClickShowEditor={handleShowEditor}
      />
      {
        pageInfo && (
          <Pagination 
            {...pageInfo}
            disabled={loading}
            current={pageConfig.page}
            total={total} 
            pageSize={pageConfig.pageSize}
            onChange={handleChange}
          />
        )
      }
    </>
  )
}

export default BaseComment;

export {EditorComment, ListComment};