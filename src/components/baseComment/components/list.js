/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { Comment, Avatar } from "antd"
import { useCallback } from "react"
import { getNumber, getTimes } from "@/utils/utils"
import cls from "classnames"
import style from "../style.module.less"
import EditorComment from "./editor"
const colors = {};
const ListComment = ({lists, onClickShowEditor = () => {}}) => {
  const getBackground = useCallback((name) => { // 组件状态更新函数不需要重新定义
    return colors[name] ? colors[name] : (colors[name] = `rgb(${getNumber(0,255)},${getNumber(0,255)},${getNumber(0,255)})`)
  }, [lists])
  // 点击回复
  const hanldShowEidtor = (item) => {
    onClickShowEditor(item); // 通知父组件修改回复属性
  }
  const handleClick = useCallback((item) => () => hanldShowEidtor(item), [])
  return (
    <div>
      {
        Array.isArray(lists) && lists.length ? lists.map(item => (
          <Comment
            key={item.id}
            author={item.name}
            avatar={<Avatar style={{background: getBackground(item.name)}}>{item.name}</Avatar>}
            actions={[
              <span>{item.userAgent}</span>,
              <span>{getTimes(item.createAt)}</span>,
              <span onClick={handleClick(item)}>回复</span>
            ]}
            content={
              <div className={cls(style.listContent)}>
                <div dangerouslySetInnerHTML={{__html: item.html}}></div>
              </div>
            }
          >
            { item.isShowEditor && <EditorComment placeholder={`回复 ${item.name}`} show={true} data={item} onClickShowEditor={onClickShowEditor} /> }
            {
              Array.isArray(item.children) && item.children.length ? <ListComment lists={item.children} onClickShowEditor={onClickShowEditor} /> : null
            }
          </Comment>
        )) : <div>暂无评论，快去发布自己的评论吧</div>
      }
    </div>
  )
}

export default ListComment;