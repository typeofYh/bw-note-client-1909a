/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { Form, Input, Button, Popover } from "antd"
import { useLocation } from "react-router-dom"
import { useTranslation } from "react-i18next"
import { SmileOutlined } from '@ant-design/icons'
import { faceHtml } from "./facePan"
import style from "../style.module.less"
import cls from "classnames"
const { TextArea } = Input;
/**
 * [EditorComment description]
 *
 * @param   {[type]}  placeholder        [placeholder 文本框默认展示内容]
 * @param   {[type]}  show               [show 收起按钮是否展示]
 * @param   {[type]}  onClickShowEditor  [onClickShowEditor 点击收起按钮触发的事件]
 * @param   {[type]}  data  [data 当前回复评论的数据对象]
 */
const EditorComment = ({placeholder = '', show = false, onClickShowEditor = () => {}, data = null }) => {
  const { t } = useTranslation();
  const { pathname } = useLocation();
  const content = (
    <div className={cls(style.facePan)}>
      <ul dangerouslySetInnerHTML={{__html: faceHtml}}></ul>
    </div>
  );
  const onFinish = (values) => {
    // console.log(values, location);
    const bodyData = {
      ...values,
      // todo: 取本地存储值
      email:'',
      hostId:'',
      name:'',
      url: pathname
    }
    if(data){ // 回复评论 不是新建
      bodyData.parentCommentId = data.id;
      bodyData.replyUserEmail = data.email;
      bodyData.replyUserName = data.name;
    }
    console.log(bodyData);
  }
  return (
    <Form
      onFinish={onFinish}
    >
      <Form.Item
        name="content"
        rules={[{ required: true, message: '请输入评论内容！' }]}
      >
        <TextArea rows={4} placeholder={placeholder}/>
      </Form.Item>
      <Form.Item className={cls(style['editor-btns'])}>
        <Popover content={content} arrowPointAtCenter>
          <Button icon={<SmileOutlined />} type="link">表情</Button>
        </Popover>
        <div>
          {show && <Button onClick={() => onClickShowEditor(data)}>收起</Button>}
          <Button htmlType="submit" type="primary">
            {t('发布')}
          </Button>
        </div>
      </Form.Item>
    </Form>
  )
}

export default EditorComment;