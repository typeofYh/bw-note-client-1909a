/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { makeAutoObservable } from "mobx"
import { getCommentList } from "@/api/article";
class CommentModule {
  comments = [];
  page = 1;
  pageSize = 6;
  async getCommentList(id, params = {page:1,pageSize:5}){
    const data = await getCommentList(id, params);
    this.comments = data.data;
    return data;
  }
}

export default makeAutoObservable(new CommentModule());